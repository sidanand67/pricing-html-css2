let toggleBtn = document.querySelector(".slider");
let priceMonthlyEl = document.querySelectorAll(".price-monthly");
let priceAnnualEl = document.querySelectorAll(".price-annually");
let isAnnual = false;

toggleBtn.addEventListener("click", () => {
    isAnnual = !isAnnual;
    if (isAnnual === true) {
        Array.from(priceMonthlyEl).forEach((element) => {
            element.style.display = "none";
        });

        Array.from(priceAnnualEl).forEach((element) => {
            element.style.display = "flex";
        });
    } else {
        Array.from(priceMonthlyEl).forEach((element) => {
            element.style.display = "flex";
        });

        Array.from(priceAnnualEl).forEach((element) => {
            element.style.display = "none";
        });
    }
});
